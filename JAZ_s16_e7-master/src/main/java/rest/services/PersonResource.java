package rest.services;

import domain.Person;
//import domain.services.*;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("people")
@Stateless
public class PersonResource {
		
	final int recordsPerPage=10;
	
	@PersistenceContext
	EntityManager em;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Person person){
		em.persist(person);
		return Response.ok(person.getId()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Person> getAll(@QueryParam("page") int pageNumber)
	{ 	
		List<Person> persons = new ArrayList<Person>();
		if (pageNumber<1){
				return persons;
			}
		else{
				persons = em.createQuery("SELECT p FROM Person p").setFirstResult((pageNumber-1)*recordsPerPage).setMaxResults(recordsPerPage).getResultList();
				return persons;
			} 
	}

	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id")int id){
		Person result = em.createNamedQuery("person.id",Person.class)
				.setParameter("personId",  id)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		em.remove(result);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id,Person p){
		Person result = em.createNamedQuery("person.id", Person.class)
				.setParameter("personId",id)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.setFirstName(p.getFirstName());
		result.setLastName(p.getLastName());
		result.setBirthday(p.getBirthday());
		result.setAge(p.getAge());
		result.setEmail(p.getEmail());
		result.setGender(p.getGender());
		return Response.ok().build();
	}
	
}
